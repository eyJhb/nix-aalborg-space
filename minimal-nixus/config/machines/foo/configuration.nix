{ lib, pkgs, config, ... }:

{
  # ... your config
  imports = [
    ./hardware-configuration.nix
  ];

  boot.loader.timeout = 10;
  boot.loader.grub.device = "/dev/vda";
  boot.kernelPackages = pkgs.linuxPackages_latest;

  users.users = {
    bob = {
      group = "users";
      isNormalUser = true;
    };
  };

  secrets.files = {
    servicePassword = {
      file = ./data/secrets/service.password;
      user = "root";
    };
  };

  system.stateVersion = "21.05";
}
