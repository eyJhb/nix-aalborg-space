let
  sources = import ./nix/sources.nix;

  # just use standard pkgs from sources
  # so that we have our applyPattches function
  pkgs = import sources.nixpkgs {};

  nixusPatched = pkgs.applyPatches {
    src = sources.nixus;
    name = "nixusPatched";
    patches = [
      # ./../patches/hm-firefox.patch
      (pkgs.fetchpatch {
        url = "https://github.com/Infinisil/nixus/pull/39.patch";
        sha256 = "0z5plm6vkvx89r2258s5z9yrxvwhclvy67scpa9v12k53rf88cq0";
      })
    ];
  };
in {
  nixusPatched = nixusPatched;
} // sources
