# { pkgs ? import <nixpkgs> {}, ... }:

let
  sources = import ../config/sources;
in import "${sources.nixusPatched}" {} ({ config, ... }: {

  defaults = { name, ... }: {
    configuration = { lib, ... }: {
      networking.hostName = lib.mkDefault name;
    };

    # use our nixpkgs from niv
    nixpkgs = sources.nixpkgs;
  };

  nodes = {
    foo = { lib, config, ... }: {
      host = "root@127.0.0.1";
      configuration = ../config/machines/foo/configuration.nix;

      # configuration = ../config/machines/foo/test.nix;
    };
  };
})
