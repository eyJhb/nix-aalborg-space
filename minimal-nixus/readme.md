# Stuff I did with niv
```
niv init
niv show
niv modify nixpkgs --branch nixos-21.05-small
niv show
niv add NixOS/nixos-hardware
niv show
niv add Infinisil/nixus
niv update
```

# build using

```
nix-build deploy
./result
```

# deployer module

I use this for initial deployment, when I have a NixOS
host, that I want to manage with nixus.
```nix
{ pkgs, lib, config, ... }:

with lib;

let
  username = "nix-deployer";
  useSudo = true;
  useDoas = true;
  sshKey =
    "ssh-ed25519 <change-me>";
in {
  services.openssh = {
    enable = true;
    openFirewall = true;
  };

  nix.trustedUsers = [ username ];

  security.sudo = mkIf useSudo {
    enable = true;
    extraRules = [{
      users = [ username ];

      commands = [{
        command = "ALL";
        options = [ "NOPASSWD" ];
      }];
    }];
  };

  security.doas = mkIf useDoas {
    enable = true;
    extraRules = [{
      users = [ username ];
      noPass = true;
      keepEnv = true;
    }];
  };

  users.users."${username}" = {
    isSystemUser = true;
    openssh.authorizedKeys.keys = [ sshKey ];
    shell = pkgs.bashInteractive;
  };
}
```

Then I include this module in the config, which I use afterwards.
Which can be thrown into the dafaults section.

```nix
{ pkgs, config, lib, ... }:

with lib;

let cfg = config.mine.deployer;
in {
  options.mine.deployer = {
    enable = mkEnableOption "Enable deployer user";

    username = mkOption {
      type = types.str;
      default = "nix-deployer";
    };

    useSudo = mkOption {
      type = types.bool;
      default = false;
    };

    useDoas = mkOption {
      type = types.bool;
      default = true;
    };

    sshKey = mkOption { type = types.str; };
  };

  config = mkIf cfg.enable {
    services.sshd.enable = true;

    nix.trustedUsers = [ cfg.username ];

    security.sudo = mkIf cfg.useSudo {
      enable = true;
      extraRules = [{
        users = [ cfg.username ];

        commands = [{
          command = "ALL";
          options = [ "NOPASSWD" ];
        }];
      }];
    };

    security.doas = mkIf cfg.useDoas {
      enable = true;
      extraRules = [{
        users = [ cfg.username ];
        noPass = true;
        keepEnv = true;
      }];
    };

    users.users."${cfg.username}" = {
      isSystemUser = true;
      openssh.authorizedKeys.keys = [ cfg.sshKey ];
      shell = pkgs.bashInteractive;
    };
  };
}
```
